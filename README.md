# README

In this repository are some tools that help with the transformation process of the .gpkg files to get the topojson files expected by Vega.js.

# INSTRUCTIONS

¡Read each program description before running!
----------------------------------------------

1.  Run `gpkg2geojson` program with its corresponding params
    
    ~~~
    // gpkg2geojson.bat <input gpkg dir> <output geojson dir>

    gpkg2geojson.bat ./gpkg/ ./geojson/
    ~~~

2.  Run `geo2topojson` program with its corresponding params:

    ~~~
    // geo2topojson.bar <feature> <input geojson dir> <output topojson dir>

    geo2topojson.bat india ./geojson/ ./topojson/
    ~~~
