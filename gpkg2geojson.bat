::===================================================================
:: # Resume
:: - Name: 			gpkg2geojson
:: - Version: 		1.0.0
:: - Description: 	The program below takes every file on a directory
::                	and does the transformation to GeoJSON format.
::
:: # Previous requirements
::   Is required to have installed OSGeo4W tool.
::   Download installer for windows https://trac.osgeo.org/osgeo4w/
:: 
:: # Parameters
:: - input_directory:	(full path)
:: - output_directory:	(full path)
:: 
:: # Configuration
:: - ext: 		the extension of the input files.
:: - osgeo4w: 	the path to the OSGeo4W tool, which has the ogr2ogr
::            	utility this program uses for the transformation.
::            	i.e: "C:\OSGeo4W64\OSGeo4W.bat"
::
::===================================================================

@echo off
echo.

echo -----------------------------
echo Running: gpkg2geojson v1.0.1
echo -----------------------------
echo.

set ext= *.gpkg

set osgeo4w= "C:\OSGeo4W64\OSGeo4W.bat"
if NOT DEFINED %osgeo4w (
    call :UndefinedVariableException "osgeo4w"
    goto :eof
)

set input_directory=%~1
if NOT DEFINED %input_directory (
    call :UndefinedVariableException "input_directory"
    goto :eof
)
echo [i] Input files are on: %input_directory%

echo.
set output_directory=%~2
if NOT DEFINED %output_directory (
    call :UndefinedVariableException "output_directory"
    goto :eof
)
echo [i] Output files will be on: %output_directory%


echo.
call :ConvertGpkgToGeoJsonSubroutine
pause
goto :eof


:::::::::::::::::::::::: subroutines ::::::::::::::::::::::::::::

:: iterates for each file on the input_directory
:ConvertGpkgToGeoJsonSubroutine
    
    set /A n= 1
    set /A count= 0

    setlocal ENABLEDELAYEDEXPANSION

    for /R %input_directory% %%f in (%ext%) do (set /A count+=1)

    for /R %input_directory% %%f in (%ext%) do (
        cls
        set /A n+=1
		echo [i] Progress: !n! of %count%
        echo ----------------------------
		call :ogr2ogr %%f
    )
    endlocal
    exit /b


:: transform the current file to geojson
:ogr2ogr
    
    setlocal
    
    set input_file= %*
    
    for %%i in (%input_file%) do set filename=%%~ni
    
    set output_file= %output_directory%%filename%.json

	echo.
    echo [i] input file: %filename%
    echo [i] output file: %output_file%
	echo.

    call %osgeo4w% ogr2ogr -f GeoJSON %output_file% %input_file%
    
    :: return to original directory
    @echo off
    call cd %~dp0
    
    endlocal
    
    exit /b

:UndefinedVariableException
    echo [!] Exception thrown by undefined parameter: %~1
    exit /b