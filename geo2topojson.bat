::===================================================================
:: # Resume
:: - Name: 			geo2topojson
:: - Version: 		1.0.0
:: - Description: 	The program below takes every file on a directory
::                	and does the transformation from GeoJSON format
::                  to TopoJSON format.
::
:: # Previous requirements
::   Is required to have installed NodeJS and the npm libraries
::   topojson, topojson-server and jsonlint.
::
::   To install the libraries, run the following command:
::      npm install -g topojson topojson-server jsonlint
:: 
:: # Parameters
:: - feature_name:      the name for the topojson feature
:: - input_directory:	(full path)
:: - output_directory:	(full path)
::
::===================================================================

@echo off
echo.

echo -------------------------
echo Running: geo2topojson v1.0.0
echo -------------------------
echo.

set feature_name=%~1
if NOT DEFINED %feature_name (
    call :UndefinedVariableException "feature_name"
    goto :eof
)
echo [i] Input files are on: %feature_name%

set input_directory=%~2
if NOT DEFINED %input_directory (
    call :UndefinedVariableException "input_directory"
    goto :eof
)
echo [i] Input files are on: %input_directory%

echo.
set output_directory=%~3
if NOT DEFINED %output_directory (
    call :UndefinedVariableException "output_directory"
    goto :eof
)
echo [i] Output files will be on: %output_directory%


echo.
call :ConvertGeoToTopoSubroutine
pause
goto :eof


:::::::::::::::::::::::: subroutines ::::::::::::::::::::::::::::

:: iterates for each file on the input_directory
:ConvertGeoToTopoSubroutine
    
    set /A n= 0
    set /A count= 0

    setlocal ENABLEDELAYEDEXPANSION

    for %%f in (%input_directory%*.json) do (set /A count+=1)

    for %%f in (%input_directory%*.json) do (
        cls
        set /A n+=1
		echo [i] Progress: !n! of %count%
        echo ----------------------------
		call :geo2topo %%f
    )
    endlocal
    exit /b

:: transform the current file to topojson
:geo2topo

    setlocal
    
    set input_file=%*

    for %%i in (%input_file%) do set filename=%%~ni
    
    set output_file= %output_directory%%filename%.json

    echo.
    echo [i] input file: %input_file%
    echo [i] output file: %output_file%
	echo.

    call geo2topo %feature_name%=%input_file% > %output_file%
    call jsonlint %output_file% -i

    :: return to original directory
    @echo off
    call cd %~dp0
    
    endlocal
    
    exit /b

:UndefinedVariableException
    echo [!] Exception thrown by undefined parameter: %~1
    exit /b